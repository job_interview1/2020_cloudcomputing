from flask import Flask
from .extensions import mongo
from .main.routes  import main
import urllib

def create_app():
    app = Flask(__name__)
    app.config['MONGO_URI'] = 'mongodb://kevin:'+ urllib.parse.quote_plus('') +'@localhost:38080/flaskapp?authSource=admin'
    mongo.init_app(app)
    app.register_blueprint(main)
    return app
